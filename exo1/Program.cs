﻿using System;

namespace exo1
{
    public class Program
    {
        public static string retour(string Phrase)
        {
            return Phrase;
        }
        public static string majoumin(string Phrase)
        {
            string retour = "Aucune données fournies.";
            if ((Phrase.Length) > 0)
            {
                int indice, affect = 0;
                string alpha_Maj = "ABCDEFGHIJKLMNOPQRSTUVWXYZ", alpha_Min = "abcdefghijklmnopqrstuvwxyz";
                for (indice = 0; indice < 26; indice++)
                {
                    if (alpha_Maj[indice] == Phrase[0])
                    {
                        retour = "Cette phrase commence par une majuscule.";
                        affect = 1;
                    }
                    if (alpha_Min[indice] == Phrase[0])
                    {
                        retour = "Cette phrase commence par une minuscule.";
                        affect = 1;
                    }
                }
                if (affect == 0)
                {
                    retour = "Cette phrase ne commence ni par une majuscule ou une minuscule.";
                }
            }
            return retour;
        }
        public static string fin(string Phrase)
        {
            string retour = "Aucune données fournies.";
            if ((Phrase.Length) > 0)
            {
                char finPhrase = Phrase[(Phrase.Length) - 1];
                if (finPhrase == '.')
                {
                    retour = "Cette phrase se termine par un point.";
                }
                else
                {
                    retour = "Cette phrase ne se termine pas par un point.";
                }
            }
            return retour;
        }
        static void Main(string[] args)
        {
            string Phrase;
            Console.WriteLine("Saisissez une phrase:");
            Phrase = Console.ReadLine();
            Console.WriteLine("Vous avez saisi : "+Program.retour(Phrase));
            Console.WriteLine(Program.majoumin(Phrase));
            Console.WriteLine(Program.fin(Phrase));
            Console.WriteLine("Appuyez sur une touche pour mettre fin au programme.");
            Console.ReadKey();
        }
    }

}
