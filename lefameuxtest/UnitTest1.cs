using System;
using Xunit;

using exo1;

namespace lefameuxtest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void retourtest()
        {
            Assert.AreEqual("Les framboises sont perchees sur le tabouret de mon grand-pere.", Program.retour("Les framboises sont perchees sur le tabouret de mon grand-pere."));
            Assert.AreEqual("azerty1234/*-", Program.retour("azerty1234/*-"));
            Assert.AreEqual("", Program.retour(""));
        }
        [TestMethod]
        public void majoumintest()
        {
            Assert.AreEqual("Cette phrase commence par une majuscule.", Program.majoumin("Les framboises sont perchees sur le tabouret de mon grand-pere."));
            Assert.AreEqual("Cette phrase commence par une minuscule.", Program.majoumin("les framboises sont perchees sur le tabouret de mon grand-pere."));
            Assert.AreEqual("Cette phrase ne commence ni par une majuscule ou une minuscule.", Program.majoumin("24zaertyuiop)^=0584"));
            Assert.AreEqual("Aucune données fournies.", Program.majoumin(""));
        }
        [TestMethod]
        public void fintest()
        {
            Assert.AreEqual("Cette phrase se termine par un point.", Program.fin("Les framboises sont perchees sur le tabouret de mon grand-pere."));
            Assert.AreEqual("Cette phrase ne se termine pas par un point.", Program.fin("Les framboises sont perchees sur le tabouret de mon grand-pere"));
            Assert.AreEqual("Cette phrase ne se termine pas par un point.", Program.fin("24zaertyuiop)^=0584"));
            Assert.AreEqual("Aucune données fournies.", Program.fin(""));
        }
    }

}
